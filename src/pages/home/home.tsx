import { Box } from '@sanch941/lib';
import { useAppDispatch, useAppSelector } from '@store';
import React, { useEffect } from 'react';
import { fetchOrders } from './model';
import { MapTemplate } from './templates/map';
import { OrdersTemplate } from './templates/orders';

export const HomePage = () => {
    const dispatch = useAppDispatch();
    useEffect(() => {
        dispatch(fetchOrders());
    }, []);
    const loading = useAppSelector(({ home }) => home.loading);

    return (
        <Box
            $height="100vh"
            fxd="column"
            $flex
            md={{ fxd: 'row', jc: 'space-between' }}
        >
            {loading === 'fulfilled' && (
                <>
                    <OrdersTemplate />
                    <MapTemplate />
                </>
            )}
        </Box>
    );
};
