import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import ordersJson from '@assets/json/orders.json';
import clientsJson from '@assets/json/clients.json';

export type OrderType = 'delivery' | 'pickup';

export interface Order {
    id: number;
    price: number;
    type: string;
    coords: { lat: number; long: number };
    client: Partial<{
        id: number;
        name: string;
        phone: string;
    }>;
}

interface InitialState {
    orders: Order[];
    loading: string;
    locationToFly: {
        lon: number;
        lat: number;
    };
}

const initialState: InitialState = {
    orders: [],
    loading: 'idle',
    locationToFly: {
        lon: null,
        lat: null
    }
};

interface SetLocationToFlyParams {
    lon: number;
    lat: number;
}

export const { reducer: homeReducer, actions: homeActions } = createSlice({
    name: 'home',
    initialState,
    reducers: {
        setLocationToFly(
            state,
            { payload }: PayloadAction<SetLocationToFlyParams>
        ) {
            state.locationToFly = payload;
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchOrders.fulfilled, (state, { payload }) => {
                state.orders = payload;
                state.loading = 'fulfilled';
            })
            .addCase(fetchOrders.pending, (state) => {
                state.loading = 'pending';
            })
            .addCase(fetchOrders.rejected, (state) => {
                state.loading = 'idle';
            });
    }
});

export const fetchOrders = createAsyncThunk<Order[]>('home/fetchOrders', () => {
    const orders: Order[] = ordersJson.map(
        (orderJson): Order => {
            const client =
                clientsJson.find(
                    (clientJson) => clientJson.id === orderJson.client_id
                ) || {};

            return {
                id: orderJson.id,
                type: orderJson.type,
                price: orderJson.price,
                coords: orderJson.coords,
                client
            };
        }
    );

    return orders;
});
