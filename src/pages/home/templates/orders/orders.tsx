import { Box, media, Text } from '@sanch941/lib';
import React from 'react';
import styled from 'styled-components';
import { List } from 'react-virtualized';
import { useAppDispatch, useAppSelector } from '@store';
import { Card } from './organisms/card';
import { homeActions } from '@pages/home/model';

export const OrdersTemplate = () => {
    const orders = useAppSelector(({ home }) => home.orders);
    const dispatch = useAppDispatch();

    return (
        <StyledContainer pTop={20} $width="100%" md={{ $width: 400 }}>
            <Text ta="center" fz={24} lh={29} fw={700} $color="#3C3C3C">
                Заявки
            </Text>

            <Box $top={20}>
                <List
                    width={isDesktop ? 400 : window.innerWidth}
                    height={window.innerHeight}
                    rowHeight={90}
                    rowCount={orders.length}
                    rowRenderer={({ key, index, style }) => {
                        const order = orders[index];

                        return (
                            <div style={style} key={key}>
                                <Card
                                    {...order}
                                    onClick={() =>
                                        dispatch(
                                            homeActions.setLocationToFly({
                                                lat: order.coords.lat,
                                                lon: order.coords.long
                                            })
                                        )
                                    }
                                />
                            </div>
                        );
                    }}
                />
            </Box>
        </StyledContainer>
    );
};

const isDesktop = window.innerWidth > 768;

const StyledContainer = styled(Box)`
    background-color: #fbfbfb;
    overflow: hidden;
    order: 2;

    ${media.md} {
        order: 1;
    }
`;
