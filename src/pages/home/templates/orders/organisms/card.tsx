import { Box, HtmlDiv, pxRem, Text } from '@sanch941/lib';
import React, { FC } from 'react';
import styled, { css } from 'styled-components';
import car from '@assets/images/car.svg';
import shop from '@assets/images/shop.svg';
import { Order } from '@pages/home/model';

export const Card: FC<ComponentProps> = ({
    type,
    client: { name } = {},
    price,
    isPopup,
    id,
    ...props
}) => {
    const { color, iconSrc, orderType } = getColorAndIconByType(type);

    return (
        <StyledContainer
            isPopup={isPopup}
            $height={80}
            $width="100%"
            $flex
            ai="stretch"
            padding={`${pxRem(15)} ${pxRem(20)} ${pxRem(15)} ${pxRem(27)}`}
            {...props}
        >
            <Box
                $height="100%"
                $flex
                ai={isPopup ? 'flex-end' : 'center'}
                $right={27}
            >
                {isPopup ? (
                    <Text fz={18} $color={color}>
                        ID: <br /> {id}
                    </Text>
                ) : (
                    <StyledIcon src={iconSrc} />
                )}
            </Box>

            <StyledDescription $flex fxd="column" jc="space-between">
                <Text fz={18} lh={22} $color={color}>
                    {name}
                </Text>
                <Text fz={14} fw={17} $color="#B1B1B1">
                    {orderType}
                </Text>
            </StyledDescription>

            <Box $left={10} $width={71} jc="flex-end" $flex ai="flex-end">
                <Text fz={20} lh={24} $color={color}>
                    {price} ₸
                </Text>
            </Box>
        </StyledContainer>
    );
};

interface ComponentProps extends Order {
    isPopup?: boolean;
    onClick?: () => void;
}

type GetColorAndIconByType = (
    type: string
) => { color: string; iconSrc: string; orderType: string };

const getColorAndIconByType: GetColorAndIconByType = (type) =>
    type === 'delivery'
        ? {
              color: '#507A2F',
              iconSrc: car,
              orderType: 'Доставка'
          }
        : {
              color: '#1F51B4',
              iconSrc: shop,
              orderType: 'Забор'
          };

const StyledContainer = styled(Box)<{ isPopup: boolean }>`
    background-color: white;
    box-shadow: 0px 10px 20px rgba(0, 0, 0, 0.05);
    border-radius: 10px;
    width: calc(100% - ${pxRem(30)});
    margin: 0 auto;
    cursor: pointer;
    transition: opacity 0.2s;

    ${({ isPopup }) =>
        isPopup
            ? css`
                  width: 100%;
              `
            : css`
                  &:hover {
                      opacity: 0.7;
                  }
              `}
`;

const StyledIcon = styled.img`
    width: ${pxRem(20)};
`;

const StyledDescription = styled(Box)`
    flex: 1;
`;
