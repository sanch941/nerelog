import React, { useEffect, useRef } from 'react';
import mapboxgl from 'mapbox-gl';
import { setupMap } from './model';
import styled from 'styled-components';
import { useAppSelector } from '@store';
import { shallowEqual } from 'react-redux';
import { media } from '@sanch941/lib';

export const MapTemplate = () => {
    const mapContainer = useRef(null);
    const map = useRef(null);
    const { orders, locationToFly } = useAppSelector(
        ({ home }) => ({
            orders: home.orders,
            locationToFly: home.locationToFly
        }),
        shallowEqual
    );

    useEffect(() => {
        if (map.current) return; // initialize map only once
        map.current = new mapboxgl.Map({
            container: mapContainer.current,
            style: 'mapbox://styles/mapbox/streets-v11',
            center: [76.889709, 43.238949],
            zoom: 12
        });

        setupMap(map.current, orders);
    }, []);

    useEffect(() => {
        const { lat, lon } = locationToFly;

        if (map.current && lat) {
            map.current.flyTo({
                center: [lon, lat],
                essential: true,
                zoom: 18
            });
        }
    }, [locationToFly.lat]);

    return <StyledContainer ref={mapContainer} className="map-container" />;
};

const StyledContainer = styled.div`
    height: 300px;
    height: 100%;
    order: 1;

    ${media.md} {
        height: 'auto';
        flex: 1;
        order: 2;
    }
`;
