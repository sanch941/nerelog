import mapboxgl from 'mapbox-gl';
import shopMarker from '@assets/images/shop-marker.png';
import carMarker from '@assets/images/car-marker.png';
import { Order } from '@pages/home/model';
import { Card } from '../orders/organisms/card';
import ReactDOMServer from 'react-dom/server';
import React from 'react';

export const setupMap = (map, orders: Order[]) => {
    map.on('load', () => {
        const features = {
            type: 'FeatureCollection',
            features: orders.map((order) => {
                return {
                    type: 'Feature',
                    properties: {
                        ...order,
                        clientName: order.client.name,
                        clientId: order.client.id,
                        clientPhone: order.client.phone
                    },
                    geometry: {
                        type: 'Point',
                        coordinates: [
                            Number(order.coords.long),
                            Number(order.coords.lat)
                        ]
                    }
                };
            })
        };

        map.addSource('orders', {
            type: 'geojson',
            // Point to GeoJSON data. This example visualizes all M1.0+ orders
            // from 12/22/15 to 1/21/16 as logged by USGS' Earthquake hazards program.
            data: features,
            cluster: true,
            clusterMaxZoom: 14, // Max zoom to cluster points on
            clusterRadius: 50 // Radius of each cluster when clustering points
        });

        setupImages(map);
        setupLayers(map);
        setupPopup(map);

        // inspect a cluster on click
        map.on('click', 'clusters', (e) => {
            const features = map.queryRenderedFeatures(e.point, {
                layers: ['clusters']
            });
            const clusterId = features[0].properties.cluster_id;
            map.getSource('orders').getClusterExpansionZoom(
                clusterId,
                (err, zoom) => {
                    if (err) return;

                    map.easeTo({
                        center: features[0].geometry.coordinates,
                        zoom
                    });
                }
            );
        });

        map.on('mouseenter', 'clusters', () => {
            map.getCanvas().style.cursor = 'pointer';
        });
        map.on('mouseleave', 'clusters', () => {
            map.getCanvas().style.cursor = '';
        });
    });
};

const setupPopup = (map) => {
    const popup = new mapboxgl.Popup({
        closeButton: false,
        closeOnClick: false,
        maxWidth: 400,
        className: 'popup'
    });

    map.on('mouseenter', 'unclustered-point', (e) => {
        // Change the cursor style as a UI indicator.
        map.getCanvas().style.cursor = 'pointer';

        // Copy coordinates array.
        const coordinates = e.features[0].geometry.coordinates.slice();
        const props = e.features[0].properties;

        // Ensure that if the map is zoomed out such that multiple
        // copies of the feature are visible, the popup appears
        // over the copy being pointed to.
        while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
            coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
        }

        const html = ReactDOMServer.renderToString(
            <Card
                {...props}
                client={{
                    id: props.clientId,
                    name: props.clientName,
                    phone: props.clientPhone
                }}
                isPopup={true}
            />
        );

        // Populate the popup and set its coordinates
        // based on the feature found.
        popup.setLngLat(coordinates).setHTML(html).addTo(map);
    });

    map.on('mouseleave', 'unclustered-point', () => {
        map.getCanvas().style.cursor = '';
        popup.remove();
    });

    map.addControl(new mapboxgl.NavigationControl());
};

export const setupImages = (map) => {
    map.loadImage(shopMarker, (error, image) => {
        if (error) throw error;
        // add image to the active style and make it SDF-enabled
        map.addImage('shop-marker', image);
    });
    map.loadImage(carMarker, (error, image) => {
        if (error) throw error;
        // add image to the active style and make it SDF-enabled
        map.addImage('car-marker', image);
    });
};

export const setupLayers = (map) => {
    map.addLayer({
        id: 'clusters',
        type: 'circle',
        source: 'orders',
        filter: ['has', 'point_count'],
        paint: {
            // Use step expressions (https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-step)
            // with three steps to implement three types of circles:
            //   * Blue, 20px circles when point count is less than 100
            //   * Yellow, 30px circles when point count is between 100 and 750
            //   * Pink, 40px circles when point count is greater than or equal to 750
            'circle-color': [
                'step',
                ['get', 'point_count'],
                '#51bbd6',
                100,
                '#f1f075',
                750,
                '#f28cb1'
            ],
            'circle-radius': [
                'step',
                ['get', 'point_count'],
                20,
                100,
                30,
                750,
                40
            ]
        }
    });

    map.addLayer({
        id: 'cluster-count',
        type: 'symbol',
        source: 'orders',
        filter: ['has', 'point_count'],
        layout: {
            'text-field': '{point_count_abbreviated}',
            'text-font': ['DIN Offc Pro Medium', 'Arial Unicode MS Bold'],
            'text-size': 12
        }
    });

    map.addLayer({
        id: 'unclustered-point',
        type: 'symbol',
        source: 'orders',
        filter: ['!', ['has', 'point_count']],
        layout: {
            'icon-image': [
                'match',
                ['get', 'type'], // Use the result 'type' property
                'delivery',
                'car-marker',
                'shop-marker'
            ],
            'icon-size': 0.6
        }
    });
};
