import { createGlobalStyle } from 'styled-components';
import interBold from '@assets/fonts/Inter/Inter-Bold.ttf';
import interExtraBold from '@assets/fonts/Inter/Inter-ExtraBold.ttf';

import interLight from '@assets/fonts/Inter/Inter-Light.ttf';
import interMedium from '@assets/fonts/Inter/Inter-Medium.ttf';

import interRegular from '@assets/fonts/Inter/Inter-Regular.ttf';
import interSemiBold from '@assets/fonts/Inter/Inter-SemiBold.ttf';

export const Fonts = createGlobalStyle`
    @font-face {
        font-family: 'Inter';
        font-weight: 300;
        src: local('Inter Light'), local('Inter-Light'),
            url(${interLight}) format('truetype'); /* Safari, Android, iOS */
    }

    @font-face {
        font-family: 'Inter';
        font-weight: 400;
        src: local('Inter Regular'), local('Inter-Regular'),
            url(${interRegular}) format('truetype'); /* Safari, Android, iOS */
    }

    @font-face {
        font-family: 'Inter';
        font-weight: 500;
        src: local('Inter Medium'), local('Inter-Medium'),
            url(${interMedium}) format('truetype'); /* Safari, Android, iOS */
    }

    @font-face {
        font-family: 'Inter';
        font-weight: 600;
        src: local('Inter Semibold'), local('Inter-Semibold'),
            url(${interSemiBold}) format('truetype'); /* Safari, Android, iOS */
    }

    @font-face {
        font-family: 'Inter';
        font-weight: 700;
        src: local('Inter Bold'), local('Inter-Bold'),
            url(${interBold}) format('truetype'); /* Safari, Android, iOS */
    }

    @font-face {
        font-family: 'Inter';
        font-weight: 700;
        src: local('Inter ExtraBold'), local('Inter-ExtraBold'),
            url(${interExtraBold}) format('truetype'); /* Safari, Android, iOS */
    }
`;
