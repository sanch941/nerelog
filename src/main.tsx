import 'core-js';
import 'regenerator-runtime/runtime';
// Из за использования webpack dll plugin, при импорта whydid you render
// через require появляется ошибка, поэтому его импортируем в es6
import React from 'react';
import ReactDOM from 'react-dom';
import { App } from '@features/app';
import mapboxgl from 'mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';

mapboxgl.accessToken =
    'pk.eyJ1Ijoic2FuY2g5NDEiLCJhIjoiY2w0YjJxOWtoMDVybTNlcWVtdWl4aHN3NiJ9.JaDCj3xokKyo7RL4Q-ofmg';

ReactDOM.render(<App />, document.getElementById('root'));
