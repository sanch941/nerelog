import { homeReducer } from '@pages/home/model';
import { combineReducers } from 'redux';

export const rootReducer = combineReducers({
    home: homeReducer
});
