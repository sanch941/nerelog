# nerelog



## Важные замечания

Добрый день, коллеги, конфиги вебпака я обычно храню в своей npm библиотеке и устанавливаю ее в своих проектах.
Если хотите посмотреть конфиги, то они здесь:
[https://gitlab.com/sanch941/configs-webpack](https://gitlab.com/sanch941/configs-webpack)

## Команды для запуска

```
1) docker build -f Dockerfile -t nerelog .
2) docker run -it -p 4001:3000 nerelog
3) Открыть http://localhost:4001/
```