FROM node:14
WORKDIR /src
COPY package.json ./
COPY yarn.lock ./
COPY ./ ./
RUN yarn
CMD ["yarn", "dev"]
